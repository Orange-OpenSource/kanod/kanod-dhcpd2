#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

import fcntl
import jinja2
from kubernetes import client
from kubernetes import config
from kubernetes import watch
import logging
import os
import socket
import struct
import subprocess
import sys
import time
import yaml

log = logging.getLogger(__name__)

logging.basicConfig(
    format='%(asctime)s %(module)s %(levelname)-10s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')


def get_ip_address(ifname):
    # return netifaces.ifaddresses(ifname)[netifaces.AF_INET][0]['addr']
    with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
        addr = socket.inet_ntoa(fcntl.ioctl(
            s.fileno(),
            0x8915,  # SIOCGIFADDR
            struct.pack('256s', bytes(ifname[:15], 'utf-8'))
        )[20:24])
    return addr


def generate_dhcp_conf(params, provisioning_itf):
    with open('/etc/dhcpd.j2', 'r') as fd:
        template = fd.read()
    ironic = params.setdefault('ironic', {})
    if 'ip' not in ironic:
        ironic['ip'] = get_ip_address(provisioning_itf)
    if 'port' not in ironic:
        ironic['port'] = '6180'
    t = jinja2.Template(template)
    print(t.render(params))
    with open('/etc/dhcpd.conf', 'w') as fd:
        fd.write(t.render(params))


def read_yaml_string(spec: str):
    return yaml.load(spec, Loader=yaml.Loader)


def update_dhcp_conf(bmh_ns):
    with open('/config/dhcp.yaml', 'r') as fd:
        dhcp_yaml_conf = yaml.safe_load(fd)

    bmh_list = client.CustomObjectsApi().list_cluster_custom_object(
        group="metal3.io", version="v1alpha1", plural="baremetalhosts",
        watch=False)

    bmh = bmh_list.get("items")
    list_bmh_hosts = []
    for x in bmh:
        if bmh_ns == "*" or bmh_ns == x["metadata"]["namespace"]:
            list_bmh_hosts.append(
                {"mac": x["spec"]["bootMACAddress"],
                    "name": x["metadata"]["name"]})

    if "hosts" not in dhcp_yaml_conf:
        dhcp_yaml_conf["hosts"] = []

    new_config_hosts_list = dhcp_yaml_conf["hosts"]
    list_mac_in_config = [d[k]
                          for d in dhcp_yaml_conf["hosts"]
                          for k in d.keys()
                          if k == "mac"]
    for bmh_host in list_bmh_hosts:
        if bmh_host["mac"] not in list_mac_in_config:
            new_config_hosts_list.append(bmh_host)

    dhcp_yaml_conf["hosts"] = new_config_hosts_list

    log.info("Update dhcpd host config with baremetalhosts : %s"
             % (list_bmh_hosts))

    return dhcp_yaml_conf


def update_config_and_launch_dhcp(proc_dhcpd, provisioning_itf, bmh_ns):
    try:
        if proc_dhcpd is not None:
            proc_dhcpd.terminate()
            log.info("dhcpd server killed")
    except Exception:
        log.error("killing dhcpd server failed")

    dhcp_yaml = update_dhcp_conf(bmh_ns)

    generate_dhcp_conf(dhcp_yaml, provisioning_itf)

    try:
        proc_dhcpd = subprocess.Popen(['/usr/sbin/dhcpd', '-f',
                                       '-d', '-4', '--no-pid',
                                       '-cf', '/etc/dhcpd.conf',
                                      provisioning_itf],
                                      stdout=sys.stdout,
                                      stderr=subprocess.STDOUT)
        log.info("dhcpd server launched with interface %s" %
                 (provisioning_itf))
    except Exception:
        log.error("error during dhcpd server launch")

    return proc_dhcpd


def run_dhcp_server():
    process_dhcpd = None

    log.info("Starting dhcp server...")
    config.load_incluster_config()
    provisioning_itf = os.environ.get('PROVISIONING_INTERFACE', 'ens4')
    bmh_namespace = os.environ.get('BMH_NAMESPACE', '*')

    try:
        subprocess.Popen(['/usr/sbin/in.tftpd',
                          '-L', '--verbose',
                          '-u', 'tftp', '--secure',
                          '/tftpboot'])
        log.info("tftp server launched")
    except Exception:
        log.error("error during tftp server launch")

    kwatch = watch.Watch()

    resource_version = None

    process_dhcpd = update_config_and_launch_dhcp(
        process_dhcpd,
        provisioning_itf,
        bmh_namespace)

    while True:
        log.info("Starting watch with resource_version %s"
                 % (resource_version))
        kwargs = {}
        kwargs['timeout_seconds'] = 3600
        kwargs['watch'] = True
        if resource_version is not None:
            kwargs['resource_version'] = resource_version

        for event in kwatch.stream(
                client.CustomObjectsApi().list_cluster_custom_object,
                "metal3.io", "v1alpha1", "baremetalhosts",
                **kwargs):
            metadata = event['object']['metadata']
            resource_version = metadata['resourceVersion']

            if bmh_namespace == "*" or bmh_namespace == metadata["namespace"]:
                if event["type"] == "ADDED":
                    log.info("event trapped : %s %s added " % (
                        event["object"]["kind"],
                        metadata["name"]))
                    process_dhcpd = update_config_and_launch_dhcp(
                        process_dhcpd,
                        provisioning_itf,
                        bmh_namespace)

                if event["type"] == "DELETED":
                    log.info("event trapped : %s %s deleted " % (
                        event["object"]["kind"],
                        metadata["name"]))
                    process_dhcpd = update_config_and_launch_dhcp(
                        process_dhcpd,
                        provisioning_itf,
                        bmh_namespace)


def main():
    with open('/config/dhcp.yaml', 'r') as fd:
        dhcp_yaml_conf = yaml.safe_load(fd)

    if dhcp_yaml_conf["subnets"] is None:
        log.info("no dhcp subnet config, dhcp server not launched")
        log.info("sleeping...")
        while True:
            time.sleep(99999)
    else:
        run_dhcp_server()


if __name__ == "__main__":
    main()
