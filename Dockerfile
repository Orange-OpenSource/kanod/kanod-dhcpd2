#  Copyright (C) 2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

FROM ubuntu:18.04 as base
LABEL project=kanod-dhcpd2

RUN apt-get update && apt-get install -y gcc binutils make perl liblzma-dev mtools git && mkdir /base && cd base && git clone --branch v1.21.1 --depth 1 https://git.ipxe.org/ipxe.git && cd ipxe/src && make -j 4 bin-x86_64-efi/ipxe.efi bin-x86_64-efi/snponly.efi bin/undionly.kpxe

FROM python:3.9-alpine
LABEL project=kanod-dhcpd2

RUN mkdir /config && apk add --no-cache tftp-hpa dhcp && \
    touch /var/lib/dhcp/dhcpd.leases && \
    adduser -D tftp && mkdir /tftpboot
    
COPY ["dhcpd.j2", "/etc/"]
COPY --from=base /base/ipxe/src/bin-x86_64-efi/ipxe.efi /tftpboot
COPY --from=base /base/ipxe/src/bin-x86_64-efi/snponly.efi /tftpboot
COPY --from=base /base/ipxe/src/bin/undionly.kpxe /tftpboot

ENV VIRTUAL_ENV=/virtual_env
RUN python3 -m venv $VIRTUAL_ENV
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

RUN pip install --no-cache-dir jinja2 && pip install --no-cache-dir PyYAML

COPY dhcp_manager /src/dhcp_manager
COPY setup.py setup.cfg MANIFEST.in /src/
WORKDIR /src
RUN pip install .
CMD ["dhcp-manager"]
