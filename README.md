# kanod-dhcpd2
This is a replacement container for `ironic-image` when launched with 
python script `dhcp-manager`.  
ISC Dhcpd and Hans Peter Anvin TFTP are used in lieu of `dnsmasq`.

The python script `dhcp-manager` watches the baremetalhosts present in the
`BMH_NAMESPACE` namespace.
When baremetalhosts are created/deleted :

- the ISC Dhcpd server is stopped
- the configuration file (file /etc/dhcpd.conf) of the Dhcpd is updated : the list of mac addresses corresponding to the existing baremetalhost are added 
  to the ``hosts`` configuration of the dhcpd server. If a host already exists in the ``hosts`` configuration part 
  with the same mac address, the configuration of this host is not modified.
- the Dhcpd server is then restarted.

The following variables are mandatory:
* `PROVISIONING_INTERFACE`: The interface on which dhcpd listens,
* `BMH_NAMESPACE`: The namespace hosting the baremetalhosts


The new iteration requires a dhcp.yaml file provided in the /config folder.
Here is an example of file:
```
ironic_ip: 192.168.5.3
ironic_port: 8090
subnets:
 - prefix: 192.168.0.0
   mask: 255.255.255.0
   start: 192.168.0.100
   end: 192.168.0.255
 - prefix: 192.168.1.0
   mask: 255.255.255.0
   start: 192.168.1.100
   end: 192.168.1.255
hosts:
- name: server1
  mac: 01:23:45:67:89:A0
  ip: 192.168.0.15
- name: server2
  mac: 01:23:45:67:89:A1
- name: server3
  mac: 01:23:45:67:89:A2
```

The first two elements are optional:
* the ironic address can be deduced from the provisionning interface
* the port for tftp has a default value for ironic (8090)

The subnet element is a list of subnets controlled by dhcp.
The `prefix`, `mask`, `start`, `end` elements are mandatory.
The `hosts` element can be used to restrict the machines handled by
the dhcp server and supply their target IP. The name is somewhat artificial
but necessary.
